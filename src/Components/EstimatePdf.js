import React from 'react'
import logo from '../Assets/oyebusy-black-logo.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faGlobe, faPhone } from '@fortawesome/free-solid-svg-icons'
import './EstimatePdf.css'
import qrcode from '../Assets/qr-code.jpg'
import { Link } from 'react-router-dom'





const EstimatePdf = () => {
  return (
    <div className='w-[1000px]  m-auto  '>
      {/* top-sec start */}
      <div className='w-full bg-[#2c3f51] h-[100px] flex justify-between  '>
        <img src={logo} className='boxed-left py-[16px]' alt='' />



        <div className='w-[80%] flex items-center	justify-end bg-[#ff9d00] rounded-bl-[104px] py-[55px] text-white text-lg'>
          <div className='text-white text-lg boxed-right flex gap-x-[42px]'>
            <div className='flex items-center text-lg	 '>
              <FontAwesomeIcon icon={faPhone} />
              <span className='pl-1 '>|</span>
              <span className='pl-1 '>9990001089</span>
            </div>
            <div className='flex items-center'>
              <FontAwesomeIcon icon={faEnvelope} />

              <span className='pl-1'>|</span>
              <span className='pl-1'>support@oyebusy.com</span>
            </div>
            <div className='flex items-center'>
              <FontAwesomeIcon icon={faGlobe} />
              <span className='pl-1'>|</span>
              <span className='pl-1'>www.oyebusy.com</span>
            </div>
          </div>
        </div>
      </div>
      <div className='w-[60%] bg-[#2c3f51] rounded-br-[104px] pt-[28px] pb-[22px] boxed-left text-white '>
        <h2 className=' text-3xl'>OyeBusy Home Service</h2>
        <h3>Book Your Service</h3>
      </div>
      {/* top-sec end */}

      <div className='flex justify-between boxed-right'>
        <div className='boxed-left'>
          <h2 className='text-2xl pt-8 pb-4 text-[#ff9d00]'>Estimate For:</h2>
          <input type='text' placeholder='Your Name' className='border' />
        </div>
        <div className='w-[30%]'>
          <h2 className='text-3xl pt-8 pb-4 text-black '>Estimate</h2>

          <div >
            <div className='flex justify-between'>
              <h4>Estimate No.:</h4>
              <input type='number' placeholder='Your Name' className='border w-28' />
            </div>


            <div className='flex justify-between pt-2' >
              <h4>Date:</h4>
              <input type='number' placeholder='Your Name' className='border' />
            </div>

          </div>
        </div>

      </div>
      {/* table */}
      <div className='invoice-itmes pt-4 bg-[url("https://i.ibb.co/xfptp7T/city-illustration-svg.png")]'>
        <table className='border-collapse border-spacing-0 border-separate ' >
          <tr className='bg-[#ff9d00] text-white border-[#dcdcdc]'>
            <th className='w-[4%] rounded-tl-2xl	 '>#</th>
            <th className='w-[50%] '>Item name</th>
            <th className='w-[11.5%] '>Quantity</th>
            <th className='w-[11.5%] '>Unit</th>
            <th className='w-[11.5%] '>Price/ Unit</th>
            <th className='w-[11.5%]  rounded-tr-2xl'>Amount</th>
          </tr>
          <tr>
            <td className='text-center'>1</td>
            <td><p className='font-bold'>Commercial Space Cleaning - Upto 900 Sq. ft. Area</p>
              Description :

              People: 3-4 | Duration: 5 - 7 Hours

              Floor cleaning using industry grade machines &
              chemicals

              Cleaning of all furnitures, doors, windows and glasses

              Includes washroom deep cleaning

              Dry dusting & cobweb removal from all areas

              Includes hard to reach area s such as lights, fans, etc</td>
            <td className='text-end pr-2'>1</td>
            <td className='text-end pr-2'>lum</td>
            <td className='text-end pr-2'>₹ 6,699.00</td>
            <td className='text-end pr-2'>₹ 6,699.00</td>

          </tr >
          <tr className='bg-[#ff9d00] text-white border-[#dcdcdc]'>
            <th></th>
            <th>Total</th>
            <th>1</th>
            <th></th>
            <th></th>
         <th>₹ 6,699.00</th>
          </tr>
          <tr>
            <td className="no-border" colSpan={"3"}></td>
            <td colSpan={"2"}>Sub Total</td>
            <td colSpan={"1"}>₹ 6,699.00</td>
          </tr>
          <tr>
            <td className="no-border" colSpan={"3"}></td>
            <td colSpan={"2"}>Exempted</td>
            <td colSpan={"1"}>-</td>
          </tr>
          <tr className='text-white border-[#dcdcdc]'>
            <th className="no-border" colSpan={"3"}></th>
            <th className='bg-[#ff9d00]' colSpan={"2"}>Total</th>
            <th className='bg-[#ff9d00]' colSpan={"1"}>₹ 6,699.00</th>
          </tr>


        </table>
      
      <div className='flex pt-11 gap-2'>
      <div >
        <img src={qrcode} alt=''/>
      </div>
      <div >
      <h2 className='text-2xl pt-8 pb-4 text-[#ff9d00]'>Pay To-</h2>
      <p>Bank Name: ICICI BANK</p>
      <p>Bank Account No.: 245005000787</p>
      <p>Bank IFSC code: ICIC0002450</p>
      <p>Bank IFSC code: ICIC0002450</p>
      <p>Account Holder Name: OYEBUSY TECHNOLOGIES PRIVATE LIMITED </p>
      </div>
      </div>
      <h2 className='text-2xl pt-8 pb-4 text-[#ff9d00]'>Estimate Amount In Words</h2>
      <p>Six Thousand Six Hundred Ninety Nine Only</p>
      <h2 className='text-2xl pt-8 pb-4 text-[#ff9d00]'>Terms And Conditions</h2>
      <ol type='1'>
        <li> The Company shall provide services to the Buyer in accordance with the Contract
applying reasonable skill and care.</li>
        <li> The Company shall use its reasonable endeavors to meet any performance dates for
the services, but any such dates shall be estimates only and time shall not be of the
essence for the performance of the services</li>
        <li> Providing facilities such as power, lighting and other such facilities or supplies
necessary to perform the services.</li>
        <li>Ensuring that the premises where the services are to be provided are free from all
health and safety hazards</li>
      
      </ol>
     
      
      
     

      <div className='mt-[260px] flex justify-end h-[35px] w-full bg-[#ff9d00]'>
        <div className='rounded-tl-[100px] mt-[-40px] h-[75px] w-[60%] bg-[#2c3f51]'></div>
      </div>
      <div className='flex justify-between boxed-padding py-[20px] bg-[white] text-lg'><span><b>Thanks for being a OyeBusy customer :</b></span><span><b>Need help? </b> support@oyebusy.com</span></div>

      </div>
    </div>
  )
}

export default EstimatePdf