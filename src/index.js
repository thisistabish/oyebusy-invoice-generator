import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import generatePDFfromHTML from './utils/HtmlToPdf';

const rootDiv = document.getElementById('root');
const root = ReactDOM.createRoot(rootDiv);

function handleDownloadPdf() {
  const htmlContent = '<h1 style="color:red;">sourabh</h1>';
  generatePDFfromHTML(rootDiv.innerHTML, 'my.pdf');
}

root.render(
  <React.StrictMode>
    <>
    <button onClick={handleDownloadPdf}>Download</button>
    <App />
    </>
  </React.StrictMode>
);


