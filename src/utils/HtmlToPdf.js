const html2pdf = require('html2pdf.js');
export default function generatePDFfromHTML(htmlContent, outputPath) {
    const element = document.createElement('div');
    element.innerHTML = htmlContent;
  
    html2pdf(element, {
      margin: 10,
      filename: outputPath,
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { scale: 2 },
      jsPDF: { unit: 'mm' } // No fixed format or orientation
    }).then(() => {
      console.log('PDF generated successfully');
    });
  }
